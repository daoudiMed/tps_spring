package com.formation.spring;

import java.util.List;
import java.util.Map;

public class GestionAdherent {
	private List<Adherent> adherents;
	private Map<String, ResponsableRayon> responsables;
	
	
	public void init() {
		System.out.println("init adherent["+this.toString()+"]");
	}
	
	public GestionAdherent() {
		super();
		// TODO Auto-generated constructor stub
	}
	public GestionAdherent(List<Adherent> adherents, Map<String, ResponsableRayon> responsables) {
		super();
		this.adherents = adherents;
		this.responsables = responsables;
	}
	/**
	 * @return the adherents
	 */
	public List<Adherent> getAdherents() {
		return adherents;
	}
	/**
	 * @param adherents the adherents to set
	 */
	public void setAdherents(List<Adherent> adherents) {
		this.adherents = adherents;
	}
	/**
	 * @return the responsables
	 */
	public Map<String, ResponsableRayon> getResponsables() {
		return responsables;
	}
	/**
	 * @param responsables the responsables to set
	 */
	public void setResponsables(Map<String, ResponsableRayon> responsables) {
		this.responsables = responsables;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "GestionAdherent [adherents=" + adherents + ", responsables=" + responsables + "]";
	}
	
	
}
