package com.formation.spring;

public class ResponsableRayon {
	private String nom,grade,rayon;

	public void init() {
		System.out.println("init adherent["+this.toString()+"]");
	}
	
	public ResponsableRayon() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ResponsableRayon(String nom, String grade, String rayon) {
		super();
		this.nom = nom;
		this.grade = grade;
		this.rayon = rayon;
	}

	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * @return the grade
	 */
	public String getGrade() {
		return grade;
	}

	/**
	 * @param grade the grade to set
	 */
	public void setGrade(String grade) {
		this.grade = grade;
	}

	/**
	 * @return the rayon
	 */
	public String getRayon() {
		return rayon;
	}

	/**
	 * @param rayon the rayon to set
	 */
	public void setRayon(String rayon) {
		this.rayon = rayon;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ResponsableRayon [nom=" + nom + ", grade=" + grade + ", rayon=" + rayon + "]";
	}
	
	
}
