package com.formation.spring.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;

import com.formation.spring.modele.Adherent;
import com.formation.spring.utils.ConnexionBD;

public class IDaoAdherentImplBD implements IDaoAdherent{
	private ConnexionBD bd =new ConnexionBD();
	private ArrayList<Adherent> liste= new ArrayList<Adherent>();
	
	@Override
	public void initAdherent() {
		// TODO Auto-generated method stub
		bd.connect();
	}
	@Override
	public Collection<Adherent> getAllAdherent() {
		// TODO Auto-generated method stub
		ResultSet rs;
		Adherent adherent;
		try {
			rs = bd.select("SELECT * FROM adherents");
			while(rs.next()) {
				adherent = new Adherent();
				adherent.setCodeAdherent(rs.getString(1));
				adherent.setNom(rs.getString(2));
				adherent.setPrenom(rs.getString(3));
				adherent.setAdresse(rs.getString(4));
				
				//System.out.println(rs.getString(1));
				liste.add(adherent);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return liste;
	}
}
