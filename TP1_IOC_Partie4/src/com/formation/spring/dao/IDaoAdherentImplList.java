package com.formation.spring.dao;

import java.util.ArrayList;
import java.util.Collection;

import com.formation.spring.modele.Adherent;

public class IDaoAdherentImplList implements IDaoAdherent{

	ArrayList<Adherent> liste = new ArrayList<Adherent>();
	
	@Override
	public void initAdherent() {
		// TODO Auto-generated method stub
		Adherent a1 = new Adherent("C200","Med","Daoudi","Ouarzazate");
		Adherent a2 = new Adherent("C300","Badr","Hary","Casa");
		liste.add(a1);liste.add(a2);
	}

	@Override
	public Collection<Adherent> getAllAdherent() {
		// TODO Auto-generated method stub
		return liste;
	}

}
