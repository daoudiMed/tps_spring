package com.formation.spring.service;

import java.util.Collection;

import com.formation.spring.modele.Adherent;
import com.formation.spring.dao.IDaoAdherent;

public class IServiceAdherentImpl implements IServiceAdherent{

	IDaoAdherent dao;
	
	public IServiceAdherentImpl() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the dao
	 */
	public IDaoAdherent getDao() {
		return dao;
	}

	/**
	 * @param dao the dao to set
	 */
	public void setDao(IDaoAdherent dao) {
		this.dao = dao;
	}

	@Override
	public void initAdherent() {
		// TODO Auto-generated method stub
		dao.initAdherent();
	}

	@Override
	public Collection<Adherent> getAllAdherent() {
		// TODO Auto-generated method stub
		return dao.getAllAdherent();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "IServiceAdherentImpl [dao=" + dao + "]";
	}
	
	
}
