package com.formation.spring;

import com.formation.spring.modele.Adherent;
import com.formation.spring.service.IServiceAdherentImpl;

import java.util.ArrayList;

import org.springframework.beans.factory.ListableBeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;

public class Main4 {
	public static void main (String[] args) {
		// Recherche du fichier de configuration de Spring
		ClassPathResource cpr=new ClassPathResource("applicationContext.xml");
		// Initialisation de la fabrique SPRING, les singletons sont charges
		ListableBeanFactory bf = new XmlBeanFactory(cpr) ;
		// Initialisation de la methode getBean en parametrant le nom du bean
		IServiceAdherentImpl sa = (IServiceAdherentImpl) bf.getBean("service");
		
		ArrayList<Adherent> adherents = (ArrayList<Adherent>) sa.getAllAdherent();
		//System.out.println(adherents.size());
		for(Adherent a : adherents) {
			System.out.println(a.getNom());
		}
		
	}
}
