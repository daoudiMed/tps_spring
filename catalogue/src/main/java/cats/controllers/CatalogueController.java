package cats.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import cats.daoSpringData.CategoryRepository;
import cats.daoSpringData.ProduitRepository;
import cats.entities.Category;
import cats.entities.Produit;


@RestController
public class CatalogueController {
	
	@Autowired 
	private ProduitRepository produitDao;
	@Autowired 
	private CategoryRepository categoryDao;
	
	@RequestMapping("/test")
	public String test(){
		return "test";
	}
	
	@RequestMapping(value="/saveCat")
	@ResponseBody
	public Category saveCategorie(Category c){
	return categoryDao.save(c);
	}
	
	@RequestMapping(value="/allCat")
	@ResponseBody
	public List<Category> allCategories(){
	return categoryDao.findAll();
	}
	
	@RequestMapping("/saveProd")
	public Produit saveProduit(Produit p){
		produitDao.save(p);
		return p;
	}
	@RequestMapping("/all")
	public List<Produit> getProduits(){
		return produitDao.findAll();
	}
	
	@RequestMapping("/produits")
	public Page<Produit> getProduits(int page){
		return produitDao.findAll(new PageRequest(page, 5));
	}
	@RequestMapping("/produitsParMC")
	public List<Produit> getProduits(String mc){
		return produitDao.produitParMC("%"+mc+"%");
	}
	
	@RequestMapping("/get")
	public Produit getProduit(Long id){
		return produitDao.findOne(id);
	}
	
	@RequestMapping("/delete")
	public boolean delete(Long id){
		produitDao.delete(id);
		return true;
	}
	
	@RequestMapping("/update")
	public Produit update(Produit p){
		produitDao.saveAndFlush(p);
		return p;
	}
	
}
