package cats.daoSpringData;

import org.springframework.data.jpa.repository.JpaRepository;

import cats.entities.Category;
import cats.entities.Produit;

public interface CategoryRepository extends JpaRepository<Category, Long>{

}
