package cats.daoSpringData;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import cats.entities.Produit;

public interface ProduitRepository extends JpaRepository<Produit, Long>{
	
	@Query("select p from Produit p where p.designation like :x")
	public List<Produit> produitParMC(@Param("x")String mc);
	
	@Query("select p from Produit p where p.designation like :x")
	public List<Produit> findByDesignation(@Param("x")String des);
	
	@Query("select p from Produit p where p.designation like :x")
	public Page<Produit> findByDesignation(@Param("x")String des,Pageable p);
}
