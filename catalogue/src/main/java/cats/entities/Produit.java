package cats.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Produit implements Serializable {
	@Id
	@GeneratedValue
	private Long id;
	
	private String designation;
	private Long reference;
	private double prix;
	
	// Constructeurs
	public Produit() {
		super();
	}
	public Produit(String designation , Long reference, double prix) {
		super();
		this.reference = reference;
		this.designation = designation;
		this.prix = prix;
	}
	// getters et Setters
	/**
	 * @return the reference
	 */
	public Long getReference() {
		return reference;
	}
	/**
	 * @param reference the reference to set
	 */
	public void setReference(Long reference) {
		this.reference = reference;
	}
	/**
	 * @return the designation
	 */
	public String getDesignation() {
		return designation;
	}
	/**
	 * @param designation the designation to set
	 */
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	/**
	 * @return the prix
	 */
	public double getPrix() {
		return prix;
	}
	/**
	 * @param prix the prix to set
	 */
	public void setPrix(double prix) {
		this.prix = prix;
	}
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Produit [id=" + id + ", designation=" + designation + ", reference=" + reference + ", prix=" + prix
				+ "]";
	}
	
}
