package cats;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import cats.daoSpringData.ProduitRepository;
import cats.entities.Produit;

@SpringBootApplication
public class CatsApplication {

	public static void main(String[] args) {
		ApplicationContext ctx = SpringApplication.run(CatsApplication.class, args);
		
		/*	Hibernat JPA test
		 * IProduitDao produitDao = ctx.getBean(IProduitDao.class);
		 */
		
		ProduitRepository produitDao = ctx.getBean(ProduitRepository.class);
		
		produitDao.save(new Produit("XL8",9000L,7));
		produitDao.save(new Produit("ACER 653",7000L,2));
		produitDao.save(new Produit("HP 85",10000L,3));
		
		List<Produit> produits = produitDao.findByDesignation("%H%");
		for (Produit p : produits) {
			System.out.println("Des: "+p.getDesignation()+" Ref:"+p.getReference()+" "+p.getPrix());
		}
		
	}

}