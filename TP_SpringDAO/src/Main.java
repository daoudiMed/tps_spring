import java.util.ArrayList;
import java.util.Map;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.formation.modele.Adherent;
import com.formation.service.*;

public class Main {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ApplicationContext bf = new ClassPathXmlApplicationContext(new String[] {"applicationContext.xml"});
	    AdherentService sm = (AdherentService) bf.getBean("gestionAdherent");
	    
	    Adherent a1=new Adherent("C100","Mohammed","Ali","Rabat");
	    Adherent a2=new Adherent("C200","Hari","Badr ","Casablanca");
	    Adherent a3=new Adherent("C300","EL Inaoui","Youness","Rabat");
	    
	    sm.addAdherent(a1);sm.addAdherent(a2);sm.addAdherent(a3);
	    
	    ArrayList<Map<String, Object>> liste = sm.getAllAdherent();
	    
	    Adherent a4 = sm.getAdherentByCode("C300");
	    a4.toString();
	    
	    sm.removeAdherent("C300");
	}
}
