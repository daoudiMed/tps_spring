package com.formation.spring;

public class Livre implements IMedia{
	
	private String titre ;
	private Auteur auteur;
	public Livre() {
		super();
	}
	public Livre(String titre, Auteur auteur) {
		super();
		this.titre = titre;
		this.auteur = auteur;
	}
	/**
	 * @return the titre
	 */
	public String getTitre() {
		return titre;
	}
	/**
	 * @param titre the titre to set
	 */
	public void setTitre(String titre) {
		this.titre = titre;
	}
	/**
	 * @return the auteur
	 */
	public Auteur getAuteur() {
		return auteur;
	}
	/**
	 * @param auteur the auteur to set
	 */
	public void setAuteur(Auteur auteur) {
		this.auteur = auteur;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Livre [titre=" + titre + ", auteur=" + auteur + "]";
	}
	@Override
	public void processusPret() {
		// TODO Auto-generated method stub
		System.out.println("Traitement du pret d'un livre "+this.toString());
	}
	
	

}
