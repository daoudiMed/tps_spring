package com.formation.spring;

public class CD implements IMedia{
	private String nom ;
	private int duree;
	
	public CD() {
		super();
	}

	public CD(String nom, int duree) {
		super();
		this.nom = nom;
		this.duree = duree;
	}

	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * @return the duree
	 */
	public int getDuree() {
		return duree;
	}

	/**
	 * @param duree the duree to set
	 */
	public void setDuree(int duree) {
		this.duree = duree;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CD [nom=" + nom + ", duree=" + duree + "]";
	}

	@Override
	public void processusPret() {
		// TODO Auto-generated method stub
		
	}
	
	
}
