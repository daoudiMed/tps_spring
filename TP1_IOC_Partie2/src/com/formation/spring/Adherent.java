package com.formation.spring;

public class Adherent {
	private String codeAdherent;
	private String nom,prenom,adresse;
	
	private IMedia media;
	
	public Adherent() {
		super();
	}

	public Adherent(String codeAdherent, String nom, String prenom, String adresse) {
		super();
		this.codeAdherent = codeAdherent;
		this.nom = nom;
		this.prenom = prenom;
		this.adresse = adresse;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Adherent [codeAdherent=" + codeAdherent + ", nom=" + nom + ", prenom=" + prenom + ", adresse=" + adresse
				+ ", media=" + media + "]";
	}

	public void init() {
		System.out.println("init adherent["+this.toString()+"]");
		
	}

	/**
	 * @return the codeAdherent
	 */
	public String getCodeAdherent() {
		return codeAdherent;
	}

	/**
	 * @param codeAdherent the codeAdherent to set
	 */
	public void setCodeAdherent(String codeAdherent) {
		this.codeAdherent = codeAdherent;
	}

	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * @return the prenom
	 */
	public String getPrenom() {
		return prenom;
	}

	/**
	 * @param prenom the prenom to set
	 */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	/**
	 * @return the adresse
	 */
	public String getAdresse() {
		return adresse;
	}

	/**
	 * @param adresse the adresse to set
	 */
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	/**
	 * @return the media
	 */
	public IMedia getMedia() {
		return media;
	}

	/**
	 * @param media the media to set
	 */
	public void setMedia(IMedia media) {
		this.media = media;
	}
	
	
}
